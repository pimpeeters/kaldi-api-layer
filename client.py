__author__ = 'tanel'

import argparse
from ws4py.client.threadedclient import WebSocketClient
import time
import threading
import sys
import urllib
import Queue
import json
import time
import os
import requests
import pattern.nl

##
## START PREP EMOTIONS
##

from sklearn.model_selection import train_test_split
from speechemotionrecognition.mlmodel import NN
from speechemotionrecognition.utilities import get_data, get_feature_vector_from_mfcc
import numpy as np
from pydub import AudioSegment
from  pydub.utils import make_chunks
import pickle

_DATA_PATH = '/opt/kaldi-api-layer/dataset'
_CLASS_LABELS = ("Neutral", "Angry", "Happy", "Sad")

def get_class_label(id):
	return str(_CLASS_LABELS[id])

def extract_data(flatten):
    data, labels = get_data(_DATA_PATH, class_labels=_CLASS_LABELS, flatten=flatten)
    x_train, x_test, y_train, y_test = train_test_split(data, labels, test_size=0.2, random_state=362)
    return np.array(x_train), np.array(x_test), np.array(y_train), np.array(y_test), len(_CLASS_LABELS)

def get_feature_vector(file_path, flatten):
    return get_feature_vector_from_mfcc(file_path, flatten, mfcc_len=39)


model_filename = "/opt/kaldi-api-layer/model.pickle"

try:
    f = open(model_filename, "r")
    model = pickle.load(f)
    f.close()

except (OSError, IOError) as e:
    x_train, x_test, y_train, y_test, _ = extract_data(flatten=True)
    model = NN()
    print("Started training model")
    model.train(x_train, y_train)
    model.evaluate(x_test, y_test)
    
    f = open(model_filename, "w")
    pickle.dump(model, f)
    f.close()

##
## END PREP EMOTIONS
##

def rate_limited(maxPerSecond):
    minInterval = 1.0 / float(maxPerSecond)
    def decorate(func):
        lastTimeCalled = [0.0]
        def rate_limited_function(*args,**kargs):
            elapsed = time.clock() - lastTimeCalled[0]
            leftToWait = minInterval - elapsed
            if leftToWait>0:
                time.sleep(leftToWait)
            ret = func(*args,**kargs)
            lastTimeCalled[0] = time.clock()
            return ret
        return rate_limited_function
    return decorate


class MyClient(WebSocketClient):

    def __init__(self, audiofile, url, protocols=None, extensions=None, heartbeat_freq=None, byterate=32000,
                 save_adaptation_state_filename=None, send_adaptation_state_filename=None):
        super(MyClient, self).__init__(url, protocols, extensions, heartbeat_freq)
        self.final_hyps = []
        self.final_con = []
        self.audiofile = audiofile
        self.byterate = byterate
        self.final_hyp_queue = Queue.Queue()
        self.final_con_queue = Queue.Queue()
        self.save_adaptation_state_filename = save_adaptation_state_filename
        self.send_adaptation_state_filename = send_adaptation_state_filename

    @rate_limited(4)
    def send_data(self, data):
        self.send(data, binary=True)

    def opened(self):
        #print "Socket opened!"
        def send_data_to_ws():
            if self.send_adaptation_state_filename is not None:
                #print >> sys.stderr, "Sending adaptation state from %s" % self.send_adaptation_state_filename
                try:
                    adaptation_state_props = json.load(open(self.send_adaptation_state_filename, "r"))
                    self.send(json.dumps(dict(adaptation_state=adaptation_state_props)))
                except:
                    e = sys.exc_info()[0]
                    #print >> sys.stderr, "Failed to send adaptation state: ",  e
            with self.audiofile as audiostream:
                for block in iter(lambda: audiostream.read(self.byterate/4), ""):
                    self.send_data(block)
            #print >> sys.stderr, "Audio sent, now sending EOS"
            self.send("EOS")

        t = threading.Thread(target=send_data_to_ws)
        t.start()


    def received_message(self, m):
        response = json.loads(str(m))
        #print >> sys.stderr, "RESPONSE:", response
        #print >> sys.stderr, "JSON was:", m
        if response['status'] == 0:
            if 'result' in response:
                trans = response['result']['hypotheses'][0]['transcript']
                if response['result']['final']:

                    align = response['result']['hypotheses'][0]['word-alignment']
                    for word in align:                       
                        self.final_con.append({
                            "word": word["word"],
                            "start": word["start"] + response["segment-start"],
                            "length": word["length"],
                            "confidence": word["confidence"]
                        })  
                    
                    
                    self.final_hyps.append(trans)
                    
            if 'adaptation_state' in response:
                if self.save_adaptation_state_filename:
                    #print >> sys.stderr, "Saving adaptation state to %s" % self.save_adaptation_state_filename
                    with open(self.save_adaptation_state_filename, "w") as f:
                        f.write(json.dumps(response['adaptation_state']))
        else:
            print >> sys.stderr, "Received error from server (status %d)" % response['status']
            if 'message' in response:
                print >> sys.stderr, "Error message:",  response['message']


    def get_full_hyp(self, timeout=60):
        return self.final_hyp_queue.get(timeout)
        
    def get_full_con(self, timeout=60):
        return self.final_con_queue.get(timeout)

    def closed(self, code, reason=None):
        #print "Websocket closed() called"
        #print >> sys.stderr
        self.final_hyp_queue.put(" ".join(self.final_hyps))
        self.final_con_queue.put(self.final_con)


def main():
	
    parser = argparse.ArgumentParser(description='Command line client for kaldigstserver')
    parser.add_argument('-u', '--uri', default="ws://127.0.0.1:80/client/ws/speech", dest="uri", help="Server websocket URI")
    parser.add_argument('-r', '--rate', default=32000, dest="rate", type=int, help="Rate in bytes/sec at which audio should be sent to the server. NB! For raw 16-bit audio it must be 2*samplerate!")
    parser.add_argument('--id', help="POST ID")
    parser.add_argument('--train-model', help="ONLY TRAIN MODEL")
    parser.add_argument('--save-adaptation-state', help="Save adaptation state to file")
    parser.add_argument('--send-adaptation-state', help="Send adaptation state from file")
    parser.add_argument('--content-type', default='', help="Use the specified content type (empty by default, for raw files the default is  audio/x-raw, layout=(string)interleaved, rate=(int)<rate>, format=(string)S16LE, channels=(int)1")
    parser.add_argument('audiofile', help="Audio file to be sent to the server", type=argparse.FileType('rb'), default=sys.stdin)
    args = parser.parse_args()

    if args.train_model is not None:
        
        sys.exit()

    else:

        content_type = args.content_type
        filenameid = args.id

        if content_type == '' and args.audiofile.name.endswith(".raw"):
            content_type = "audio/x-raw, layout=(string)interleaved, rate=(int)%d, format=(string)S16LE, channels=(int)1" %(args.rate/2)


        filename = args.audiofile.name.replace(".mp3", ".frag.wav")
        sound = AudioSegment.from_mp3(args.audiofile.name)
        sound = sound.set_channels(1).set_frame_rate(16000)

        chunk_length_ms = 5000
        chunks = make_chunks(sound, chunk_length_ms) #Make chunks of ten sec

        predictions = []

        for chunk in chunks:
            chunk.export(filename, format="wav")
            predictions.append(get_class_label(model.predict_one(get_feature_vector_from_mfcc(filename, flatten=True)).sum().sum()))


        # Creating an empty dictionary  
        summary = {
            "Neutral": 0,
            "Angry": 0,
            "Happy": 0,
            "Sad": 0
        } 

        for item in predictions: 
            if (item in summary): 
                summary[item] += 1.0
            else: 
                summary[item] = 1.0

        max_val = 0
        max_key = "Neutral"
        for key, value in summary.items(): 
            summary[key] = value / len(predictions)
            if value / len(predictions) > max_val:
                max_val = value / len(predictions)
                max_key = key

        emotion = {
            "predicted_emotion": max_key,
            "emotionflow": predictions,
            "emotions": summary
        }


        ## KALDI HERE

        ws = MyClient(args.audiofile, args.uri + '?%s' % (urllib.urlencode([("content-type", content_type)])), byterate=args.rate,
                    save_adaptation_state_filename=args.save_adaptation_state, send_adaptation_state_filename=args.send_adaptation_state)
                    
        ws.connect()

        fulltext = ws.get_full_hyp().encode('utf-8')
        sentiment = pattern.nl.sentiment(fulltext)
        predictedSentiment = 'neutral'
        if sentiment[0] > 0.4:
            predictedSentiment = 'positive'
        elif sentiment[0] < -0.2:
            predictedSentiment = 'negative'

        result = {
            "file": args.audiofile.name,
            "text": fulltext,
            "words": ws.get_full_con(),
            "sentiment": {
                "score": sentiment[0],
                "subjectivity": sentiment[1],
                "predicted_sentiment": predictedSentiment
            },
            "emotion": emotion
        }

        sum_conf = 0.0
        for word in result["words"]:
            sum_conf = sum_conf + word["confidence"]

        result["avg_conf"] = sum_conf / len(result["words"])

        # with open("output.txt", "w+") as f:
        #     f.write(json.dumps(result))
        #     f.close()

        r = requests.post(url = os.environ['CALLBACK_URI'] + filenameid + "/", json = result) 
        print(r)

if __name__ == "__main__":
    main()

