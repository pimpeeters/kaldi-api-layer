import sys
import subprocess
import time
import os

from flask import Flask, jsonify, request
app = Flask(__name__)


from werkzeug.utils import secure_filename



@app.route('/', methods=['GET', 'POST'])
def upload_file():
	if request.method == 'POST':
		# check if the post request has the file part
		if 'file' not in request.files:
			return jsonify({"success": 0})

		file = request.files['file']
		if file.filename == '':
			return jsonify({"success": 0})

		filenameid = request.form["id"]

		_, file_extension = os.path.splitext(file.filename)
		filename = str(int(round(time.time() * 1000))) + file_extension
		file.save(os.path.join("/opt/kaldi-api-layer/upload", filename))
		os.system("python /opt/kaldi-api-layer/client.py --id " + filenameid + " \"/opt/kaldi-api-layer/upload/" + filename + "\" &")
		return jsonify({"success": 1, "filename": filename})

	return jsonify({"success": 0})

if os.environ['CALLBACK_URI']:

	if __name__ == '__main__':
		app.run(host="0.0.0.0", port=3000)

else:
	print("No callback URI found")



# if len(sys.argv) < 2:
# 	print("Need folder name")
# 	exit()

	
# folder = sys.argv[1]


# files = glob.glob(folder + "/" + "*.mp3")
# files.extend(glob.glob(folder + "/" + "*.wav"))

# i = 0;

# print(files)

# print("Started transcribing directory")
# start = time.time()

# for file in files:
	
# 	file = file.replace("\\", "/");
	
# 	i = i + 1;
# 	print("Transcribing file " + str(i) + "/" + str(len(files)) + " (" + file + ")");
	
	
# 	p = subprocess.call("python client.py \"" + file + "\"", shell=True)
# 	time.sleep(5)


# end = time.time()
# print("Finished transcribing in " + str(end - start) + " seconds")